# 2023 Finals Challenge List

|Category|Name|Difficulty|
|:----|:----|:----|
|Trivia|[Respect Your Elders](2023/finals/trivia/respect-your-elders)|Easy|
|Trivia|[History of Hacks: Sony](2023/finals/trivia/history-of-hacks-sony)|Easy|
|Trivia|[Hack the Vote](2023/finals/trivia/hack-the-vote)|Easy|
|Trivia|[Knowledge is Power](2023/finals/trivia/knowledge-is-power)|Easy|
