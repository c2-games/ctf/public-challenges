# 2023 Regionals Challenge List

|Category|Name|Difficulty|
|:----|:----|:----|
|Trivia|[Achy Breaky Heartbleed](2023/regionals/trivia/achy-breaky-heartbleed)|Easy|
|Trivia|[Infosec News (3)](2023/regionals/trivia/infosec-news-3)|Easy|
|Trivia|[Mobile Malware](2023/regionals/trivia/mobile-malware)|Easy|
|Trivia|[PQ Psych](2023/regionals/trivia/pq-psych)|Easy|
