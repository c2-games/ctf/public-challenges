# NCAE Cyber Games Public Challenges

## Description

This repository contains a list of all challenges that have been released by NCAE Cyber Games for public use. These
 challenges were either part of the Regional or Final competition, or are part of the training sandbox. The purpose
 of this repository is to allow anyone that wants to practice historical challenges the ability to do so in their
 own environment.

## Structure

The repository is structured such that all challenges are listed in a top-table table by competition year, then
 listed by regionals & finals events. Each of the regionals and finals folders will have a list of challenges
 used during the respective event, broken down by category. Use the main table to identify the year/event of
 interest and use the links to access the challenge contents.

Interactive challenge Docker images, used in either the regional or final events are available in the container
 repository if you aren't looking to build them manually. All challenges contained in this repository include notes
 for solving the challenge, source code to allow challenge modification for re-use (where applicable), as well as
 instructions for building and deploying the challenge as needed.

## Usage

TODO

## Challenge Listing

### 2023 Competition Year Challenges

[2023 Regional Challenges](2023/regionals/README.md)  
[2023 Finals Challenges](2023/finals/README.md)

### 2022 Competition Year Challenges

[2022 Regional Challenges](2022/regionals/README.md)  
[2022 Finals Challenges](2022/finals/README.md)

## Troubleshooting

If you are having a problem with any of the challenges in this repository, you can open an issue or contact us at
 ctf-content@ncaecybergames.org
