# Challenge Creation Notes
The test-site folder contains the files used to create a temporary SSL HTTP container to serve a site. If you want to change the flag, edit the index.html page and modify the flag. This only changes the flag! You will still have to do a capture of web traffic and load the relevant SSL keys in order to serve up another version of this challenge. There have been five (5) versions of the challenge created with different flags to be used for now. 

The following URL has information about how to capture SSL keys on a Windows or Linux system.
https://unit42.paloaltonetworks.com/wireshark-tutorial-decrypting-https-traffic/

After setting the environment variable, visit the site container and capture the request to / using Wireshark. Then, capture the SSL keys and PCAP and use those as the challenge files.

# Solving
To solve, the challenge.pcapng file should be opened in wireshark. This will show the encrypted traffic captured between the user and the server. The user then loads the ssl-keys.log file by clicking Edit->Preferences->Protocols (dropdown)->TLS and then loading the log file in the "(Pre)-Master-Secret log filename option. After loading this, the HTTP traffic will be decrypted and the user can click on one of these packets and select Follow->HTTP Stream or Follow->TLS Stream. This will show all decrypted traffic. Then, using the "Find" search bar at the bottom, the flag can be searched for. The user can also just read through the decrypted traffic and look for the flag. 
