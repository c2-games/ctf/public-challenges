# 2022 Regionals Challenge List

|Category|Name|Difficulty|
|:----|:----|:----|
|Trivia|[Feeling Much Better, Thanks](2022/regionals/trivia/feeling-better-thanks)|Easy|
|Trivia|[For Sparta!](2022/regionals/trivia/for-sparta)|Easy|
|Trivia|[Hacker History](2022/regionals/trivia/hacker-history)|Easy|
|Trivia|[More Hacker History](2022/regionals/trivia/hacker-history-2)|Easy|
|Trivia|[Hack the Planet!](2022/regionals/trivia/hack-the-planet)|Easy|
|Trivia|[Hard CIDR](2022/regionals/trivia/hard-cidr)|Easy|
|Trivia|[In The Beginning...](2022/regionals/trivia/in-the-beginning)|Easy|
|Trivia|[Know Your Hashes](2022/regionals/trivia/know-your-hashes)|Easy|
|Trivia|[SearchSploit](2022/regionals/trivia/searchsploit)|Easy|
|Trivia|[What the RFC?](2022/regionals/trivia/what-the-rfc)|Easy|
|Trivia|[You're Making Me Sick!](2022/regionals/trivia/youre-making-me-sick)|Easy|
|Crypto|[Secure Storage](2022/regionals/crypto/secure-storage)|Medium|
|Crypto|[Secure in Transit](2022/regionals/crypto/secure-in-transit)|Medium|
