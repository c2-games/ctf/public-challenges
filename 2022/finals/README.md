# 2022 Finals Challenge List

|Category|Name|Difficulty|
|:----|:----|:----|
|Trivia|[Infosec News (1)](2022/finals/trivia/infosec-news-1)|Easy|
|Trivia|[Infosec News (2)](2022/finals/trivia/infosec-news-2)|Easy|
|Trivia|[Oh Ja?](2022/finals/trivia/oh-ja)|Easy|
|Trivia|[FTP Hax](2022/finals/trivia/ftp-hax)|Easy|
|Crypto|[Messy Message](2022/finals/crypto/messy-message)|Easy|
|Crypto|[What A Conundrum](2022/finals/crypto/what-a-conundrum)|Easy|
|Crypto|[Shall We Play A Game?](2022/finals/crypto/shall-we-play-a-game)|Medium|
